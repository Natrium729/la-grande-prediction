"La Grande Prédiction ou l'Astrologue étourdi" by Natrium729

[
Pour faire bref :
Ce code source est libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la « GNU General Public License » telle que publiée par la Free Software Foundation : soit la version 3 de cette licence, soit toute version ultérieure.

Ce programme est distribué dans l'espoir qu'il vous sera utile, mais SANS AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ ni d'ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.

Pour faire long, consultez : http://www.gnu.org/licenses
]

Volume - Initialisation

Use full-length room descriptions.
The maximum score is 1.

Book - Extensions

Include French by Eric Forgeot. Use French 1PSPa Language.

Include Basic Screen Effects by Emily Short.


Book - Verbes

Part - Destinée du souverain

The kingdestiny is a kind of value. The kingdestinies are mort, souffrance, maladie and gloire.

Predicting is an action applying to one kingdestiny.
Understand "[kingdestiny]" as predicting.

Check predicting:
	if the location is not Destinée du souverain, say "Pardon ?" instead.

Carry out predicting:
	now the annonce1 is the kingdestiny understood;
	say "Notre bon souverain connaîtra la [kingdestiny understood] !";
	wait for any key;
	now the player is in Destinée de la cité.

The prédiction1 is a kingdestiny that varies.
When play begins:
	now the prédiction1 is a random kingdestiny between mort and maladie.

The annonce1 is a kingdestiny that varies.


Part - Destinée de la cité

The citydestiny is a kind of value. The citydestinies are incendie, guerre civile, ruine and prospérité.
Understand "prosperite" as prospérité.

Citypredicting is an action applying to one citydestiny.
Understand "[citydestiny]" as citypredicting.

Check citypredicting:
	if the location is not Destinée de la cité, say "Pardon ?" instead.

Carry out citypredicting:
	now the annonce2 is the citydestiny understood.

Report citypredicting:
	say "Notre belle cité connaîtra [if the citydestiny understood is incendie]l['][else]la [end if][citydestiny understood] !".

The prédiction2 is a citydestiny that varies.
When play begins:
	now the prédiction2 is a random citydestiny between incendie and ruine.

The annonce2 is a citydestiny that varies.


Book - État initial

Understand "robe" as yourself when the location is not place.
Understand "manteau" as yourself when the location is place.


Part - Barre de statut

When play begins:
	now the right hand status line is "[turn count]".

Part - Things

Chapter - Trousseau de clefs

The trousseau de clefs unlocks the porte d'entrée.
The description of the trousseau de clefs is "L'ensemble des clefs ouvrant toutes les portes de la tour. Il est très précieux[first time]. Il me manquait une clef. Je me demandais où elle était[only].".
The indefinite article of the trousseau de clefs is "mon".
Understand "trousseau", "clef", "clefs", "cle" or "cles" as the trousseau de clefs.
Instead of dropping the trousseau de clefs, say "J'en aurais sûrement eu besoin.".

Chapter - Clef de la cave

The clef de la cave unlocks the porte de la cave.
The clef de la cave is female.
The indefinite article of the clef de la cave is "la".
The description is "Une vieille clef toute rouillée qui je n'avais jamais utilisée.".
Understand "clef" or "cle" as the clef de la cave.


Part - Backdrops

Chapter - Escalier

The escalier is backdrop. The escalier is in escalier1, escalier2, escalier3, escalierc and escalierrdc.
The description of the escalier is "Un long et tortueux escalier en colimaçon traversait toute la pièce, du plafond jusqu'au sol. Quelle corvée de le monter du rez-de-chaussée au sommet de la tour !".
Understand "marches" or "marche" as the escalier.

Instead of entering the escalier, say "Ce n[']était pas le moment de m'asseoir.".


Part - Understanding

Chapter - Porte

Understand "porte" as a door.

Chapter - Directions

Understand "h" as up.
Understand "b" as down.


Volume - Jeu

Book - Introduction

When play begins:
	now the description of the player is "Aujourd'hui est le grand jour : je porte le manteau de gala que j'ai tant rêvé de mettre. Saurais-je le porter avec honneur ?";
	say "Je suis le plus grand astrologue du monde.";
	wait for any key;
	say "Bon, peut-être de la ville.";
	wait for any key;
	say "D'accord, sûrement de mon quartier seulement.";
	wait for any key;
	say "Et encore.";
	wait for any key;
	say "Quoi qu'il en soit, aujourd'hui est un grand jour : la Fête de la Fondation va commencer. Chaque année, un astrologue doit annoncer à la foule en liesse les prédictions concernant notre bon souverain, et par la même occasion la bonne marche de notre cité.";
	wait for any key;
	say "[line break]Cette année, c'est moi qui dois annoncer la Grande Prédiction…[line break]";
	wait for any key.


Book -  Prologue

Part - Place

The place is a room. "La Grand-Place noire de monde. Le bruit était impressionnant il y a quelques secondes, mais maintenant, tout le monde retient son souffle ; seuls les pleurs d'un bébé viennent renforcer le silence. On n'attend que moi. Je devrais m'avancer plutôt que de faire durer ce supplice.".
The printed name of the place is "Devant la foule silencieuse".

The player is in the place.

Instead of doing something other than looking, examining, smelling or entering when the location is place, say "Non, il n'est pas temps de me défiler."

Instead of listening to the place:
	say "Rien, pas un son hormis ces pleurs incessants qui me stressent encore plus.".

Chapter - Foule

The foule is female scenery in the place.
Understand "monde", "gens" or "personnes" as the foule.
The description of the foule is "Des milliers de regards rivés sur l'estrade vide. Personne ne veut manquer ce grand événement. Enfin, si, peut-être moi…".

Chapter - Bébé

The bébé is scenery in the place.
Understand "bebe" as the bébé.
The description of the bébé is "Je ne parviens pas à le voir dans cette foule compacte. Et je ne le chercherai pas : voir tous ces regards qui m'attendent est une torture.".

Instead of listening to the bébé, say "Ce bébé qui pleure ne comprend vraiment rien. C'est moi qui devrait être en train de pleurer.".

Chapter - Estrade

The estrade is a enterable supporter in the place. "L'estrade vide m'attend.".
The description of the estrade is "Une estrade drapée de velours rouge et encadrée d'une barrière dorée. Je devrais monter.".

Instead of upgoing when the location is place, try entering the estrade.

After entering the estrade:
	say "Ça y est, c'est le moment. Le moment ou jamais. Qu'avais-je prédit déjà ?";
	wait for any key;
	say "…";
	wait for any key;
	say "[line break]…";
	wait for any key;
	say "[line break]Non ! Je ne sais plus ! J'avais pourtant étudié le ciel cette nuit ! Qu'avais-je vu ?! Je dois me souvenir !";
	wait for any key;
	clear only the main screen;
	now the player carries the trousseau de clefs;
	now the player is in the hall;
	now the description of the player is "Je portais ma robe pourpre d'astrologue, attribut de ma profession. Elle n[']était pas toute neuve.".
	

Book - Première partie

Part - Rez-de-chaussée

Chapter - Hall

The hall is a room. "Le hall de ma modeste tour d'astrologie. À vrai dire, je l'avais héritée de mon père. Jamais je n'aurais pu l'acheter. Ce jour-ci, le carrelage était encore plus brillant que d'habitude et les miroirs tapissant littéralement les murs donnaient — heureusement — un aspect de grandeur.[line break][one of]Au mur, une horloge venait de sonner seize heures[or]Il était environ seize heures[stopping]. Que faisais-je déjà à ce moment-là ? [first time]Je n'allais pas sortir. [only]Peut-être voulais-je prendre l'escalier qui se situait à l'ouest.".
The printed name of the hall is "Dans le hall de ma tour".

Section - Horloge

The horloge is female scenery in the hall.
Understand "aiguille", "aiguilles" or "balancier" as the horloge.
The description of the horloge is "Il s'agissait d'une belle horloge datant de plusieurs générations et son fier balancier ne s[']était jamais arrêté. L'aiguille annonçait qu'il était environ seize heures. À moins qu'elle fût restée bloquée…".

Instead of opening the horloge, say "Je n'y connaissais vraiment rien en mécanique et encore moins en horlogerie. J'évitai donc d'y toucher.".

Instead of attacking the horloge, say "Je frappai l'horloge un bon coup, espérant lui faire regagner sa régularité. Cela marcha-t-il ?".

Instead of listening to the horloge, say "Je collai mon oreille sur l'horloge, et entendit un tic-tac irrégulier. Étrange…[line break]".

Section - Miroirs

Some miroirs is scenery in the hall.
Understand "miroir", "mur" or "murs" as the miroirs.
The description of the miroirs is "De splendides miroirs recouvrant les murs. J'aime à m'y contempler. Ce jour-là, je portais ma magnifique robe pourpre d'astrologue, quelque peu rapiécée."

Section - Carrelage

The carrelage is scenery in the hall.
Understand "carreaux", "carreau" or "sol" as the carrelage.
The description of the carrelage is "Ornant le sol et soigneusement astiqués par ma domestique chaque jour, on pouvait y voir son propre reflet.".

Section - Porte d'entrée

The porte d'entrée is a female locked lockable openable door. The porte d'entrée is outside from the hall and inside from the rue.
The indefinite article of the porte d'entrée is "la".
Understand "porte" as the porte d'entrée.
Understand "heurtoir" as the porte d'entrée when the location is rue.
The description of the porte d'entrée is "Une double porte en chêne massif aux magnifiques ornements métalliques[if the location is rue]. Un lourd heurtoir en forme d[']étoile filante attendait qu'on le soulève[end if].".

Instead of unlocking the door with the trousseau de clefs when the étagères are not searched, say "Pourquoi aller dehors maintenant ?".

Instead of taking the porte d'entrée when the location is rue, say "Je soulevai le heurtoir et frappai quelques coups à la porte. J'attendis, mais personne n'ouvrit.".

Instead of taking the porte d'entrée when the location is rue and the domestique is not working and the domestique is not tired:
	say "Je soulevai le heurtoir et le laissai retomber. J'entendis des pas derrière la porte, et la domestique apparut.";
	now the domestique is in the rue.

Instead of attacking the porte d'entrée, try taking the porte d'entrée.


Chapter - Escalier

The escalierrdc is west from the hall. "Un long escalier en colimaçon traversait de part en part la pièce. Monter m'aurait mené dans les étages supérieurs, alors que descendre… Non, j'avais trop peur de la cave.[line break]Le hall était à l'est.".
The printed name of the escalierrdc is "Devant une volée de marches".


Part - Premier étage

Chapter - Escalier

The escalier1 is up from the escalierrdc.  "Un long escalier en colimaçon traversait de part en part la pièce. Monter m'aurait fait rejoindre la bibliothèque. J'aurais sinon pu retourner au rez-de-chaussée.[line break]Les cuisines étaient à l'ouest, la salle à manger au nord et la chambre de la domestique à l'est.".
The printed name of the escalier1 is "Devant une volée de marches".

Chapter - Cuisines

The cuisines is west from the escalier1. "[if dirty]Les cuisines étaient très sales : étrange, c[']était le boulot de la domestique. Je devais lui demander de nettoyer les armoires, pour commencer.[else]Les cuisines étaient désormais dans un état admirable. Voilà quand la domestique obéit ![end if][line break]La sortie était à l'est.".
The printed name of the cuisines is "Dans les cuisines".
The cuisines can be dirty or clean. The cuisines is dirty.

Instead of exiting in the cuisines, try going east.

Section - Armoires

Some armoires are fixed in place closed not openable female container in the cuisines.
The description of the armoires is "[if the cuisines is dirty]Des armoires dégoûtantes rien que de les voir de l'extérieur. Et je ne voulais vraiment pas les ouvrir[else]Les armoires furent parfaitement nettoyées par cette domestique paresseuse[end if].".
Understand "armoire" as the armoires.

Instead of rubbing or opening the armoires when the cuisines is dirty:
	say "Jamais je ne me serais sali les mains dans ce bourbier immonde !".

Instead of smelling the armoires, say "Non. Il s'agissait là d'une très, très mauvaise idée.".

Section - Briquet

The briquet is a thing in the armoires.
The description of the briquet is "Un couple de pierres que ma domestique utilisait pour allumer du feu afin de cuisiner.".


Chapter - Salle à manger

The salle à manger is north from the escalier1. "La somptueuse salle à manger contenait une grande table, bien que je fusse le seul à y prendre les repas : la domestique mangeait dans sa chambre et il était trop coûteux de recevoir des invités. Ahhh, à quand la gloire et l'argent…[line break]La sortie était au sud.".
The printed name of the salle à manger is "Dans la salle à manger".

Instead of exiting in the salle à manger, try going south.

Section - Table

The table is a female supporter in the salle à manger.
The indefinite article of the table is "la grande".
Understand "nappe" as the table.
The description of the table is "Une longue et large table, avec une simple nappe blanche. Je m'asseyais toujours au bout, pour me donner une impression de grandeur.".

Instead of pulling the table, say "Ce n'était pas une bonne idée.".
Instead of entering the table, say "Cela ne faisait pas partie des bonnes manières."
Instead of climbing the table, try entering the table.

Section - Chandelier

The chandelier is a switched off device on the table. "Un chandelier trônait sur la table.".
The description of the chandelier is "Un chandelier sobre en métal que je mettais habituellement sur la table. Ses trois bougies étaient presque entièrement fondues, mais je n'en avais plus : la cire coûte cher.".
Understand "bougies", "bougie", "chandelles" or "chandelle" as the chandelier.

Check an actor switching on the chandelier:
	if the player can not touch the briquet, say "Je n'avais rien pour l'allumer." instead.

After switching on the chandelier:
	now the chandelier is lit;
	say "J'allumai le chandelier avec le briquet.".

After switching off the chandelier:
	say "Je soufflai le chandelier.";
	now the chandelier is not lit.


Chapter - Chambre de la domestique

The chambred is east from the escalier1. "Une pièce rudimentaire pour les domestiques. Il n'y avait qu'un lit, car je n'ai pas assez d'argent pour avoir plusieurs servantes sous mes ordres.[line break]L'escalier était à l'ouest.".
The printed name of the chambred is "Dans la chambre de la domestique".

Instead of exiting in the chambred, try going west.

Section - Lit

The bedd is scenery in chambred.
Understand "lit" as the bedd.
The description is "[if the domestique is tired]La domestique y ronflait[else]Après s'être réveillée, la domestique avait fait son lit.".

Section - Domestique

[description]
The domestique is a female women in the chambred. "La domestique [if tired]dormait à poings fermés[else if working]nettoyait la cuisine d'arrache-pied[else]attendait mes ordres[end if].".
The description of the domestique is "Ma seule domestique n[']était qu'une paresseuse, mais faute de moyens, elle m[']était quand même utile. Comment s'appelait-elle, en fait ?".

Instead of talking to the tired domestique, say "Elle dormait.".
Instead of listening to the tired domestique, say "Elle ronflait.".
Instead of kissing the domestique, say "J[']étais son maître, tout de même !".

Instead of searching the domestique, say "Pourquoi faire ? Elle ne possédait rien de particulier.".

Persuasion rule for asking the domestique to try doing something:
	if the domestique is tired
	begin;
		say "Elle n'entendit pas.";
		persuasion fails;
	else if the domestique is working;
		say "Elle était déjà occupée à autre chose.";
		persuasion fails;
		else;
		persuasion succeeds;
	end if.


[état] 
The domestique can be tired. The domestique is tired.
The domestique can be working. The domestique is not working.

[réveil]
Instead of pushing the domestique, try waking the domestique.
Instead of waving the domestique, try waking the domestique.
Instead of waking the tired domestique, say "Je la secouai vivement, mais elle ne se réveilla pas.".

Understand "gifler [something]" as attacking.

Instead of attacking the tired domestique:
	say "Je n'avais pas d'autres moyens de la réveiller, et même si je haïssais la violence, je levai la main, et la giflai. Elle se réveilla en sursaut. Elle faillit dire quelque chose, mais se ravisa : on n'insultait pas son maître ![paragraph break][italic type]Vous pouvez donner des ordres à la domestique avec des commandes type « Domestique, [bracket]action (à l'infinitif)[close bracket] »[roman type].";
	now the domestique is not tired.

[obéissance autres]
Instead of answering the not tired domestique that, say "Il aurait été stupide de lui demander cela.".
Instead of asking the domestique to try doing something other than going, say "Il aurait été stupide de lui demander cela.".

[obéissance déplacement]
After the domestique going a direction (called D):
	say "La domestique obéit et alla [if D is west or D is east]à l['][else if D is up or D is down][else]au [end if][D].".

[nettoyage armoires]
Understand "laver [something]" as rubbing.

Instead of asking the domestique to try rubbing the armoires for the first time:
	say "La domestique obéit (non sans une grimace) et se dirigea vers les rangements, armée jusqu'aux gencives de tout l'attirail du parfait petit nettoyeur. Et elle se mit au travail.";
	now the domestique is working;
	The cleaning finishes in ten turns from now;
	stop the action.

At the time when the cleaning finishes:
	now the cuisines is clean;
	now the armoires are openable;
	if the location is the cuisines, show the key.

After going to the clean cuisines when the clef de la cave is off-stage for the first time:
	show the key;
	continue the action.

[Clef de la cave]
To show the key:
	say "Quel merveilleux travail ! Quand la domestique s'y mettait, quel boulot ! J'aurais peut-être dû lui donner une prime… Et puis non, en fait.";
	wait for any key;
	say "La voilà qui approcha ; elle ne venait pas pour la prime, elle me tendit une clef, en me disant qu'elle l'avait trouvée en nettoyant la cuisine. La clef de la cave !";
	wait for any key;
	now the domestique is not working;
	now the player carries the clef de la cave.


Part - Deuxième étage

Chapter - Escalier

The escalier2 is up from the escalier1. "J[']étais à l[']étage de la bibliothèque. Ici, pas de murs et dans toutes les directions, des livres. Le papier coûte cher, c'est vrai, mais lire est ma deuxième passion après l'astrologie.[line break]Je pouvais monter jusqu[']à ma chambre, ou descendre.".
The printed name of the escalier2 is "Devant une volée de marches".

After going to the escalier2 when the étagères are searched for the first time:
	say "Kling !";
	wait for any key;
	say "Ne perdant pas de temps, je sortis mes clefs. Mais dans ma précipitation, je fis tomber le trousseau !";
	wait for any key;
	say "Kling, kling, kling, kling !";
	wait for any key;
	say "On aurait dit qu'il était tombé jusqu[']à la cave. Oh non !";
	wait for any key;
	now the rat carries the trousseau de clefs;
	continue the action.

Chapter - Bibliothèque

The bibliothèque is north from the escalier2, northeast from the escalier2, east from the escalier2, southeast from the escalier2, south from the escalier2, southwest from the escalier2, west from the escalier2, northwest from the escalier2.
The description of the bibliothèque is "Des étagères se dressaient partout, on ne savait plus où poser les yeux. C[']était la seule pièce dotée d'une certaine majesté[first time]. Où était donc mon exemplaire du [italic type]Grand Livre du Ciel[roman type] ? J'en avais besoin pour la Grande Prédiction[only].".
The printed name of the bibliothèque is "Parmi des dizaines d[']étagères".

Instead of exiting in the bibliothèque, try going north.

Section - Étagères

Some étagères are scenery in the bibliothèque.
The description of the étagères is "Des centaines d[']étagères croulant sous des centaines de livres[if the étagères are not searched]. Le [italic type]Grand Livre du Ciel[roman type] devait être ici[end if].".
Understand "etagere", "etageres", "livre" or "livres" as the étagères.

Instead of taking the étagères, say "Je n'avais pas le temps de lire. Il valait mieux chercher celui dont j'avais besoin.".

The étagères can be searched. The étagères are not searched.

Instead of searching the not searched étagères:
	say "Je fouillai et fouillai les étagères, en quête de mon livre.";
	wait for any key;
	say "Enfin, le voilà. Je me saisis de lui et… il tomba en poussière. Les rats !";
	wait for any key;
	say "Noon ! Il fallait vite que j'achète un autre livre à la librairie !";
	wait for any key;
	now the étagères are searched.


Part - Troisième étage

Chapter - Escalier

The escalier3 is up from the escalier2. "J[']étais presque au sommet. Plus haut se trouvait mon observatoire. Ici, ma chambre se trouvait au sud. Le reste n[']était que rangements et pièces inutilisées. Je pouvais aussi redescendre à la bibliothèque.".
The printed name of the escalier3 is "Devant une volée de marches".

Chapter - Chambre

The chambre is south from the escalier3. "Ma chambre était (très) en désordre, mais c[']était tout à fait normal, car je n'autorisais pas ma domestique à y entrer. Moi-même y entrant peu souvent, le désordre s[']était installé petit à petit.[line break]L'escalier était au nord.".
The printed name of the chambre is "Dans ma chambre à coucher".

Instead of exiting in the chambre, try going north.

Section - Désordre

The désordre is scenery in the chambre.
Understand "desordre" as the désordre.
The description of the désordre is "Un fouillis indescriptible de choses diverses, que je n'avais le courage de ranger.".

Section - Lit

The bed is scenery in the chambre.
Understand "lit" as the bed.
The description of the bed is "Mon lit. Il était à peu près fait.".

Section - Portemanteau

The portemanteau is scenery in the chambre.
The description of the portemanteau is "Un portemanteau sobre, qui me permettait d'y suspendre mon précieux manteau.".

Section Manteau

The manteau is an open not openable container in the chambre. "L'un de mes manteaux était accroché à un portemanteau.".
The description of the manteau is "Mon manteau de gala, que je n'avais mis qu'une fois. En effet, on ne m'invitait pas souvent (jamais en vérité, je ne l'avais mis que pour l'essayer). Il me semble que j'y avais glissé quelque chose dans la poche.".
Understand "poche" as the manteau.

Instead of wearing the manteau, say "Je portais déjà quelque chose, et je le gardais pour une grande occasion ; si elle se présentait un jour.".

Before searching the manteau:
	now the printed name of the bourse is "bourse";
	now the indefinite article of the bourse is "une";
	now the bourse is female.
	
Before taking the bourse:
	now the printed name of the bourse is "bourse";
	now the indefinite article of the bourse is "une";
	now the bourse is female.

Section - Bourse

The bourse is a male thing in the manteau.
The indefinite article of the bourse is "quelque".
Understand "quelque chose", "chose", "argent", "piece" or "pieces" as the bourse.
The description of the bourse is "Une bourse tintant quand on la prenait. Pas beaucoup, certes, mais juste de quoi acheter un livre.".
The printed name of the bourse is "chose".

Instead of searching the bourse, say "Elle contenait quelques pièces, rien de plus.".
Instead of opening the bourse, say "Je gardais l'argent pour un achat important.".


Part - Quatrième étage

Chapter - Dôme

The dôme is a up from the escalier3. "Un gigantesque dôme de verre qui donnait une vue à trois cent soixante degrés sur la ville. C'est d'ici, avec le grand télescope, que je scrute le ciel afin de créer mes prédictions. Il n[']était pas encore l'heure[unless the player carries the livre] et je n'avais pas le [italic type]Grand Livre du Ciel[roman type][end unless].".
The printed name of the dôme is "Sous le dôme de verre".

Instead of waiting when the location is the dôme:
	if the player carries the livre
	begin;
	say "J'attendis longtemps, jusqu[']à la nuit. Enfin, quand les étoiles se levèrent, je scrutai le ciel.";
	wait for any key;
	say "Je vis d'abord la constellation [if the prédiction1 is mort]de la croix ; d'après le livre, elle signifiait la mort[else if the prédiction1 is souffrance]du scorpion ; d'après le livre, elle signifiait la souffrance[else if the prédiction1 is maladie]du rat ; d'après le livre, elle signifiait la maladie[end if].";
	wait for any key;
	say "Ensuite vint la constellation de [if the prédiction2 is incendie]la flamme ; elle signifiait l'incendie[else if the prédiction2 is guerre civile]la hache ; elle signifiait la guerre civile[else if the prédiction2 is ruine]la pièce d'or, traversée par Jupiter ; cela signifiait la ruine[end if].";
	wait for any key;
	say "[line break]Ça y est, je m'en souviens ! Je vais pouvoir annoncer la Prédiction ! J'ouvre les yeux et émerge de mes pensées. La foule est toujours silencieuse et encore plus impatiente. C'est le moment !";
	wait for any key;
	clear only the main screen;
	now the player is in the Destinée du souverain;
	else;
	say "Inutile, je n'avais pas le livre.";
	end if.

Section - Ville

The ville is scenery in the dôme.
Understand "dome", or "ciel" as the ville.
The description of the ville is "À travers le dôme, je pouvais voir la ville s[']étendre à mes pieds et sous le ciel sans nuage. Il était encore assez tôt, et l'on remarquait quelques passants minuscules parcourir les rues.".

Section - Télescope

The télescope is scenery in dôme.
Understand "telescope" as the télescope.
The description of the télescope is "Un énorme télescope, poli et rutilant, visait le ciel. C[']était également un héritage et il devait valoir une fortune. Cependant, je n'avais aucune intention de le vendre, il m[']était trop précieux.".

Instead of searching the télescope:
	try waiting.
	

Part - Sous-sol

Chapter - Escalier

The escalierc is down from the escalierrdc. "L'escalier ne descendait pas plus bas. Heureusement, car cet endroit me donnait la chair de poule. Les araignées, les rats, les serpents…[line break]La cave était à l'est.".
The escalierc is dark.
The printed name of the escalierc is "Devant une volée de marches".

Instead of going to the escalierc from the escalierrdc when the rat does not carry the trousseau de clefs, say "Je n'avais aucune raison valable de descendre à la cave et je détestais cet endroit.".

Section - Porte de la cave

The porte de la cave is a closed openable locked lockable female door. "La porte de la cave me dominait, menaçante.".
The porte de la cave is east from the escalierc and west from the cellier.
The description of the porte de la cave is "Une porte noire, à moitié pourrie par l'humidité[first time], attendait que je l'ouvre[only][if the location is escalierc]. Qu'allais-je découvrir derrière ? Euh, la cave, sûrement[end if].".


Chapter - Cave

The description of the cellier is "Un sombre endroit, qui était malgré tout éclairé par un maigre soupirail. L'humidité ici était suffocante. La cave était vide, je n'y avais jamais mis les pieds. Et je ne les y mettrais plus.[line break]La sortie était à l'ouest."
The printed name of the cellier is "Dans les entrailles de la Terre (la cave, quoi)".

Section - Rat

The rat is an animal in cellier. "Un rat me regardait sans aucune crainte.".
The description of the rat is "Un énorme rongeur effrayant, avec des incisives comme des rasoirs.[if the rat carries the trousseau de clefs] Il portait le trousseau de clefs ![end if] Je ne savais pas ce qui me retenait de fuir.".

Instead of attacking the rat, say "J'avais bien trop peur de lui pour lui faire du mal !".

[parler]
Instead of talking to the rat, say "Il avait l'air de me comprendre[if the rat carries the trousseau de clefs]. On aurait dit qu'il voulait une autre clef en échange[end if]. Je secouai la tête. N'importe quoi, je parlais à un rat…[line break]"

[clef]
Instead of dropping the clef de la cave in the presence of the rat, try giving the clef de la cave to the rat.

Instead of giving the clef de la cave to the rat:
	say "Il s'empara aussitôt de la clef en laissant échapper le trousseau !";
	now the trousseau de clefs is in the cellier;
	now the rat carries the clef de la cave;
	if the porte de la cave is locked
	begin;
		say "J'avais le trousseau de clef, et il fallait que je sortisse de la cave. Je pris la poignée et me rendis compte que la porte était verrouillée et que je n'avais plus la clef. J[']étais bloqué à tout jamais dans la cave !";
		wait for any key;
		end the story saying "Ça ne s[']était pas passé comme ça !";
	end if.

[fuite]
Every turn when the location is cellier:
	if the rat was in the cellier and the player carries the switched on chandelier
	begin;
		now the rat is off-stage;
		say "Le rat s'enfuit en me voyant arriver avec mon chandelier allumé.";
	end if;
	if the rat was not in the cellier and (the chandelier is switched off or the chandelier is not in the location)
	begin;
		now the rat is in the cellier;
		say "En voyant la luminosité diminuer, le rat revint.";
	end if.


Part - Ville

Chapter - Rue

The description of the rue is "Une petite rue qui débutait à partir de ma tour et se prolongeait vers le nord-ouest. Elle était calme : les préparatifs pour la fête du lendemain étaient terminés et tout le monde se reposait.".
The printed name of the rue is "Le long d'une rue".

Chapter - Librairie

The librairie is northwest from the rue. "J'avais suivi la rue pour arriver sur une petite place où se situait ma librairie favorite ; j[']étais maintenant à l'intérieur. J'adorais cet endroit et l'odeur qu'il dégageait.".
The printed name of the librairie is "Chez le marchand de livres".

Instead of smelling the librairie, say "Il y avait une odeur particulière ici. L'odeur de la connaissance.".

Instead of exiting in librairie, try going southeast.

Section - Libraire

The libraire is a man in the librairie. "Le libraire sourit en me reconnaissant.".
Understand "vendeur", "marchand" or "homme" as the libraire.
The description of the libraire is "Le vieil homme barbu et moustachu me regardait avec bienveillance. C[']était un homme bon, mais il était intraitable en affaires.".

Instead of talking to the libraire, say "« Alors, qu'est-ce qui vous amène ? »[line break]"

Instead of giving the bourse to the libraire:
	say "Je sortis ma bourse et en tirai quelques pièces. Le libraire secoua la tête : il en voulait plus. À regret, je pris les pièces restantes et les lui donnai. « Voilà votre livre ! » me dit-il avec un grand sourire.";
	now the libraire carries the bourse;
	now the player carries the livre.

Section - Livre

The livre is a thing in the librairie. "Un [italic type]Grand Livre du Ciel[roman type] était sur le comptoir."
The description of the livre is "[if the libraire carries the bourse]Maintenant que je possédais le livre, il fallait que je retournasse à l'observatoire.[else]On aurait dit que le riche volume ouvragé m'attendait. Vite, j'en avais besoin ![end if]".
The printed name of the livre is "[italic type]Grand livre du Ciel[roman type]".
The indefinite article of the livre is "un exemplaire du".
Understand "grand livre du ciel", "exemplaire", "volume" or "ouvrage" as the livre.

Instead of buying the livre in the presence of the libraire when the libraire does not carry the bourse:
	if the player carries the bourse, try giving the bourse to the libraire;
	else say "Je n'avais pas d'argent.".

Instead of taking the livre in the presence of the libraire when the libraire does not carry the bourse, say "Le vendeur secoua le doigt en signe d'interdiction. Il fallait que je paie…[line break]".

Carry out the domestique taking the livre for the first time:
	increase the score by one.

After the domestique taking the livre, say "La domestique prit le livre discrètement alors que je la cachais.".

Instead of reading the livre, say "Je n'avais pas encore besoin de le lire.".


Book - Troisième partie

Part - Destinée du souverain

The Destinée du souverain is a room. "Qu'ont annoncé les étoiles quant à la destinée de notre souverain ?[paragraph break]• Mort ;[line break]• Gloire ;[line break]• Souffrance ;[line break]• Maladie.".

Instead of doing something other than predicting or looking when the location is Destinée du souverain, say "Je dois annoncer ma prédiction.".

Part - Destinée de la cité

The Destinée de la cité is a room. "Qu'ont annoncé les étoiles quant à la destinée de notre cité ?[paragraph break]• Incendie ;[line break]• Guerre civile ;[line break]• Ruine ;[line break]• Prospérité.".

Instead of doing something other than citypredicting or looking when the location is Destinée de la cité, say "Je dois annoncer ma prédiction.".

Part - Final

To end:
	wait for any key;
	if annonce1 is gloire and annonce2 is prospérité
	begin;
		say "Oh non ! J'ai dit n'importe quoi ! Mais à ma grande surprise, un applaudissement retentit, puis un autre, et encore un autre !";
		wait for any key;
		say "La voix du roi résonne alors : « Je fais de toi mon astrologue personnel ! » Ça alors !";
		wait for any key;
		say "Je comprends ! L'astrologie n'est pas une science, l'astrologie n'a toujours été que mensonge ! Il faut raconter des bobards si l'on veut se faire croire ! Tant pis, maintenant, je suis astrologue royal !";
		wait for any key;
		end the story finally saying "Je suis l'astrologue royal !";
	else if prédiction1 is annonce1 and prédiction2 is annonce2;
		say "Ça y est, j'ai annoncé la Grande Prédiction. Mais à ma grande surprise, je vois la foule se mettre en colère.";
		wait for any key;
		say "La voix du roi retentit alors : « Décapitez-moi ce traître ! » Mais… mais je ne comprends pas : ma prédiction était correcte !";
		wait for any key;
		end the story saying "Je vais être décapité demain, à l'aube";
	else if prédiction1 is not annonce1 or prédiction2 is not annonce2;
		say "Oh non ! J'ai dit n'importe quoi ! La foule se met en colère, et la voix du roi retentit : « Décapitez-moi ce traître ! »";
		wait for any key;
		end the story saying "Je vais être décapité demain, à l'aube";
	end if.


Volume - Releasing

Release along with cover art, a solution, the source text and a file of "Changelog" called "LGPOLAE-changelog.txt".

Book - Bibliographic data

The story headline is "Une supercherie interactive".
The story genre is "Comédie".
The release number is 1.
The story description is "Bien qu'astrologue modeste, vous avez été choisi pour annoncer la Grande Prédiction lors de la Fête de la Fondation. Mais alors que vous allez prendre la parole, un trou se creuse dans votre mémoire. Allez-vous pouvoir refaire votre journée d'hier dans votre tête en quelques secondes, avant que la foule ne s'impatiente ? Vous souviendrez-vous de ce qu'avait indiqué le Grand Livre du Ciel ?".
The story creation year is 2012.


Book - Index map

Index map with place mapped east from hall.


Book - Commandes

Part - Aide

After printing the banner text, say "[first time]Tapez « aide » pour obtenir plus d'informations sur le jeu ou si vous jouez à une fiction interactive pour la première fois.[only]".

Understand "aide", "infos", "info", "informations" or "information" as a mistake ("Ce jeu est une fiction interactive ; il se joue en tapant des commandes à l'infinitif telles que « voir », « prendre [italic type]objet[roman type] », etc. (pour plus d'informations, consultez [italic type]www.ifiction.free.fr[roman type]).[line break]Tapez « licence » pour l'obtenir et « auteur » pour en savoir plus sur l'auteur.").

Part - Licence

Understand "licence" or "license" as a mistake ("[bold type]Pour faire bref :[line break][roman type]Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la « GNU General Public License » telle que publiée par la Free Software Foundation : soit la version 3 de cette licence, soit toute version ultérieure.[paragraph break]Ce programme est distribué dans l'espoir qu'il vous sera utile, mais SANS AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ ni d'ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.[paragraph break][bold type]Pour faire long, [roman type]consultez : [italic type]http://www.gnu.org/licenses[roman type].[paragraph break]Le livre de la couverture, du Dr. Marcus Gossler, est sous la licence Creative Commons BY-SA ([italic type]www.creativecommons.org/licenses/by-sa/3.0/deed.fr[roman type]) et a été trouvée sur Wikimedia.").

Part - Auteur

Understand "credits", "credit", "auteur" or "auteurs" as a mistake ("Il était écrit dans les étoiles que moi, Natrium729, écrirais ce jeu. Depuis ce jour, je scrute le ciel tous les soirs, à la recherche d'un quelconque message de leur part. Elles ne m'ont rien dit, alors qu'elles auraient pu me contacter à [italic type]natrium729@gmail.com[roman type] ou sur mon site, [italic type]http://ulukos.com[roman type] (où se trouve d'ailleurs une solution pour ceux qui n'arrive pas à lire le ciel).[paragraph break]Je suis né sous une bonne étoile pour avoir eu mes bêta-testeurs, qui se reconnaîtront.").