# Journal des modifications de *la Grande Prédiction ou l'Astrologue étourdi*

## Unreleased (00/00/00)


### Objets

- Nouvel objet : bébé (place).
- Nouvel objet : lit (chambre de la domestique).
- Nouvel objet : désordre (chambre).
- Nouvel objet : lit (chambre).
- Synonymie : « manteau » est maintenant un synonyme du joueur uniquement dans la place.
- Synonymie : « robe » est maintenant un synonyme du joueur uniquement s'il n'est pas dans la place.
- Synonymie : « heurtoir » est maintenant un synonyme de la porte d'entrée uniquement lorsque le joueur est dans la rue.

### Actions

- On peut maintenant sentir dans la place.
- Monter dans la place fait maintenant monter le joueur sur l'estrade.
- Ouvrir l'horloge ne produit plus la réponse par défaut.
- Attaquer l'horloge ne produit plus la réponse par défaut.
- Écouter l'horloge ne produit plus l'horloge par défaut.
- Tirer la table ne produit plus la réponse par défaut.
- Monter sur la table ne produit plus la réponse par défaut.
- Parler à la domestique endormie ne produit plus la réponse par défaut.
- Écouter la domestique ne produit plus la réponse par défaut.
- Embrasser la domestique ne produit plus la réponse par défaut.
- Fouiller la domestique ne produit plus la réponse par défaut.
- Secouer la domestique ne produit plus la réponse par défaut.
- Donner des ordres à la domestique ne produit plus de réponses inappropriées.
- Sentir dans la librairie ne produit plus la réponse par défaut.
- Donner la clef de la cave au rat alors que la porte de la cave est verrouillée fait maintenant perdre la partie.
- Prendre la porte d'entrée ("soulever le heurtoir") ne fait plus venir la domestique si elle est en train de dormir ou de nettoyer les cuisines.
- Frapper la porte d'entrée fait prendre la porte d'entrée.
- On peut maintenant prédire la prospérité en tapant « prosperite ».

### Textes

- Correction de diverses coquilles.
- Après l'annonce de la prédiction, les messages quand le joueur gagne ou perd ont changé.
- La commande « auteur » a changé.

### Code

- Quelques optimisations.

### Autre

Il y a aussi eu d'autres modifications qui n'ont pas été notées.



## Release 1 (22/01/2012)

Première version.
